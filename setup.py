from setuptools import setup

setup(
    name='epytext2rst',
    version='1.0.2',
    url='https://bitbucket.org/mrleeh/epytext2rst',
    license='GPL',
    author='Stefan Lehmann',
    author_email='Stefan.St.Lehmann@gmail.com',
    description='Convert epytext markup language in RestructuredText',
    scripts=['epytext2rst.py']
)
