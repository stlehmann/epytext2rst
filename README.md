# epytext2rst

Command line tool to convert epytext markup language in RestructuredText.

# Usage

    epytext2rst.py [-h] [-v] [-o OUTPUT] input

    positional arguments:
      input                 input file or directory to convert

    optional arguments:
      -h, --help            show this help message and exit
      -v, --verbose         show changes
      -o OUTPUT, --output-dir OUTPUT
                            directory for output of parsed files

To convert all python files (*.py, *.pyw) in a directory just type this:

``python epytext2rst.py mydirectory -o newdirectory``

If you just want to know what gets substituted do this:

``python epytext2rst.py mydirectory -v``

# Convention:

*epytext2rst*  handles the conversion of the following Epytext components:

## Keywords

    @(param|type|rtype|return|ivar) --> :(param|type|rtype|return|ivar)

## Italics    

    I\{(.*?)\}    --> *..*
    
## Bold

    B\{(.*?)\}    --> **..**
    
## Code

    C\{(.*?)\}    --> ``...``